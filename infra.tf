locals {
  dockerfile = "Dockerfile.custom_image"
  ecr_endpoint = aws_ecr_repository.python_lambda.repository_url
  tag = md5(format("%s%s", filemd5("${path.root}/${local.dockerfile}"), filemd5("${path.root}/run.py")))
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_ecr_repository" "python_lambda" {
  name                 = "python_lambda"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
}

data "aws_ecr_authorization_token" "token" {}

resource "null_resource" "build_docker_image" {
  triggers = {
    dockerfile = filemd5("${path.root}/${local.dockerfile}")
    code = filemd5("${path.root}/run.py")
    tag = local.tag
  }

  provisioner "local-exec" {
    working_dir = path.root
    command = <<EOF
docker login -u AWS -p ${nonsensitive(data.aws_ecr_authorization_token.token.password)} ${local.ecr_endpoint} && \
docker build -t ${local.ecr_endpoint}:${local.tag} -f ${local.dockerfile} . && \
docker push ${local.ecr_endpoint}:${local.tag}
EOF
  }

  depends_on = [
    aws_ecr_repository.python_lambda
  ]
}

resource "aws_lambda_function" "python_lambda" {
  function_name = "python_lambda"
  timeout       = 5 # seconds
  image_uri     = "${aws_ecr_repository.python_lambda.repository_url}:${local.tag}"
  package_type  = "Image"

  role = aws_iam_role.python_lambda.arn

  depends_on = [ null_resource.build_docker_image ]
}

resource "aws_iam_role" "python_lambda" {
  name = "python_lambda"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}