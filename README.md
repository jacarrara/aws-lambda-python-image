# AWS Lambda Python image
Test code to deploy with Terraform an AWS Lambda with a docker image by using Python runtime.

## Requirements
- Docker
- AWS account

# How to run locally?
- Select the dockerfile flavor and edit it in infra.tf file
- With the docker service turned on run:
```bash
terraform init
terraform apply
```
- Enjoy! :D

# License
MIT