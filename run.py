import yaml

def run():
    members = [{'name': 'Zoey', 'occupation': 'Doctor'},
            {'name': 'Zaara', 'occupation': 'Dentist'}]

    print(yaml.dump(members))

def handler(event, context):
    run()
    return {
        "status": "ok"
    }